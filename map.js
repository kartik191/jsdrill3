function map(elements, cb) {
    if(!Array.isArray(elements)){
        throw 'Input is not an Array';
    }
    let result = [];
    for(let i = 0; i < elements.length; i++){
        result[i] = cb(elements[i], i, elements);
    }
    return result;
}

module.exports = map;