const reduce = require('../reduce');


const items = [1, 2, 3, 4, 5, 5]; 
let result;

try{
    result = reduce(items, (startingValue, CurrValue) => startingValue + CurrValue, 0);
    console.log(result);
}
catch(e){
    console.error(e);
}


try{
    result = reduce(items, function (startingValue, CurrValue){return CurrValue%2 === 0 ? startingValue + 1 : startingValue; });
    console.log(result);
}
catch(e){
    console.error(e);
}


try{
    result = reduce(items, (startingValue, CurrValue) => startingValue + CurrValue, undefined);
    console.log(result);
}
catch(e){
    console.error(e);
}


try{
    result = reduce('items', (startingValue, CurrValue) => startingValue + CurrValue, 0);
    console.log(result);
}
catch(e){
    console.error(e);
}


try{
    result = reduce(items, '(startingValue, CurrValue) =>  startingValue + CurrValue', undefined);
    console.log(result);
}
catch(e){
    console.error(e);
}