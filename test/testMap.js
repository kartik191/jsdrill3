const map = require('../map');

const items = [1, 2, 3, 4, 5, 5]; 
let result;

try{
    result = map(items, (element) =>  element + 1);
    console.log(result);
}
catch(e){
    console.error(e);
}


try{
    result = map(items, (element) =>  element * element);
    console.log(result);
}
catch(e){
    console.error(e);
}



try{
    result = map(items, (element) =>  element * 2);
    console.log(result);
}
catch(e){
    console.error(e);
}


try{
    result = map(items,);
    console.log(result);
}
catch(e){
    console.error(e);
}


try{
    result = map('items',);
    console.log(result);
}
catch(e){
    console.error(e);
}


try{
    result = map([],);
    console.log(result);
}
catch(e){
    console.error(e);
}


try{
    result = map(items, function(element){});
    console.log(result);
}
catch(e){
    console.error(e);
}