const flatten = require('../flatten');


let nestedArray;
let result;
try{
    nestedArray = [1, [2], [[3]], [[[4]]]];
    result = flatten(nestedArray);
    console.log(result);
}
catch(e){
    console.error(e);
}


try{
    nestedArray = [1, [2], [[3],14,15], [[[4,5,6],7,8,9],10,11],12,13];
    result = flatten(nestedArray, 2);
    console.log(result);
}
catch(e){
    console.error(e);
}


try{
    nestedArray = [1, 2, 3, 4, 5, 6, 7, 8];
    result = flatten(nestedArray);
    console.log(result);
}
catch(e){
    console.error(e);
}


try{
    nestedArray = [0, 1, 2, [[[3, 4]]]];
    result = flatten(nestedArray, 4);
    console.log(result);
}
catch(e){
    console.error(e);
}


try{
    nestedArray = [0, 1, 2, [[[3, 4]]]];
    result = flatten(nestedArray, undefined);
    console.log(result);
}
catch(e){
    console.error(e);
}


try{
    nestedArray = [0, 1, 2, [[[3, 4]]]];
    result = flatten(nestedArray, 'hello');
    console.log(result);
}
catch(e){
    console.error(e);
}


try{
    nestedArray = [0, 1, 2, [[[3, 4]]]];
    result = flatten(nestedArray, NaN);
    console.log(result);
}
catch(e){
    console.error(e);
}


try{
    nestedArray = [0, 1, 2, [[[3, 4]]]];
    result = flatten(nestedArray, 'hello');
    console.log(result);
}
catch(e){
    console.error(e);
}


try{
    nestedArray = ['abc', 1, {1: 'hello', 2: 'world'}, [[[3, 4]]]];
    result = flatten(nestedArray, 1.99999999999999999);
    console.log(result);
}
catch(e){
    console.error(e);
}


try{
    result = flatten('nestedArray', 'hello');
    console.log(result);
}
catch(e){
    console.error(e);
}