const filter = require('../filter');


function isEven(element){
    return element%2 ? true : false; 
}

function isOdd(element){
    return element%2 === 0 ? true : false; 
}

function equalTo5(element){
    return element === 5 ? true : false; 
}


const items = [1, 2, 3, 4, 5, 5], items1 = [1, 2, 3, 4, 5, 5, 5, 5, 7, 8, 1, 45, 23, 54, 1, 1] ; 
let result;

try{
    result = filter(items, isEven);
    console.log(result);
}
catch(e){
    console.error(e);
}


try{
    result = filter(items1, isOdd);
    console.log(result);
}
catch(e){
    console.error(e);
}


try{
    result = filter(items1, equalTo5);
    console.log(result);
}
catch(e){
    console.error(e);
}


try{
    result = filter(items1, items1);
    console.log(result);
}
catch(e){
    console.error(e);
}


try{
    result = filter('items1', items1);
    console.log(result);
}
catch(e){
    console.error(e);
}


try{
    result = filter(items1, (elements, i) => elements[i] + 10);
    console.log(result);
}
catch(e){
    console.error(e);
}