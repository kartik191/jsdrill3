const each = require('../each');


const items = [1, 2, 3, 4, 5, 5]; 

try{
    each(items, (element, index) => { console.log(element); });
    console.log('\n');
}
catch(e){
    console.error(e);
}


try{
    each(items, (element, index) => { console.log(element); });
    console.log('\n');
}
catch(e){
    console.error(e);
}


try{
    each('items', (element, index) => { console.log(element); });
    console.log('\n');
}
catch(e){
    console.error(e);
}


try{
    each(items, (element, index, array) => { console.log(array[index]); });
    console.log('\n');
}
catch(e){
    console.error(e);
}


try{
    each(items, (element, index) => console.log(element, index));
    console.log('\n');
}
catch(e){
    console.error(e);
}


