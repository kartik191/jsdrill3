const find = require('../find');


const items = [1, 2, 3, 4, 5, 5]; 
let result;

try{
    result = find(items, (element) =>  element > 2);
    console.log(result);
}
catch(e){
    console.error(e);
}


try{
    result = find(items, (element) =>  {if(element == 5) return true; else return false;});
    console.log(result);
}
catch(e){
    console.error(e);
}

try{
    result = find(items);
    console.log(result);
}
catch(e){
    console.error(e);
}


try{
    result = find('items', (element) =>  element > 2);
    console.log(result);
}
catch(e){
    console.error(e);
}