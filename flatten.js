function flatten(elements, depth){
    if(!Array.isArray(elements)){
        throw 'Input is not an Array';
    }
    if(!Number.isFinite(depth)){
        depth = 1;
    }
    let result = [];
    for(let i = 0; i < elements.length; i++){
        if(Array.isArray(elements[i]) && depth >= 1){
            let flatElement = flatten(elements[i], depth - 1);
            result = result.concat(flatElement);
        }
        else{
            result.push(elements[i]);
        }
    }
    return result;
}

module.exports = flatten;