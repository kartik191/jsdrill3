function find(elements, cb) {
    if(!Array.isArray(elements)){
        throw 'Input is not an Array';
    }
    for(let i = 0; i < elements.length; i++){
        if(cb(elements[i], i, elements) === true){
            return elements[i];
        }
    }
    return undefined;
}

module.exports = find;