function reduce(elements, cb, startingValue) {
    if(!Array.isArray(elements)){
        throw 'Input is not an Array';
    }
    let prevValue = startingValue, i = 0;
    
    if(typeof(startingValue) === 'undefined'){
        if(elements.length > 0){
            prevValue = elements[0];
        } 
        i = 1;
    }
    for(i; i < elements.length; i++){
        prevValue = cb(prevValue, elements[i], i, elements);
    }
    return prevValue;
}

module.exports = reduce;