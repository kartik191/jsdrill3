function each(elements, cb) {
    if(!Array.isArray(elements)){
        throw 'Input is not an Array';
    }
    for(let i = 0; i < elements.length; i++){
        cb(elements[i], i, elements);
    }
}

module.exports = each;