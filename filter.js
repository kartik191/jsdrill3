function filter(elements, cb) {
    if(!Array.isArray(elements)){
        throw 'Input is not an Array';
    }
    let result = [];
    for(let i = 0; i < elements.length; i++){
        if(cb(elements[i], i, elements) === true){
            result.push(elements[i]);
        }
    }
    return result;
}


module.exports = filter;